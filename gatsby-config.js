module.exports = {
  siteMetadata: {
    title: `Empathy Minds`,
    author: `Calvin Gonzalez`,
    description: `A personal site and blog powered by Gatsby and Markdown`,
    siteUrl: `https://callygonzo.gitlab.io/personal-site/`,
    social: {
      twitter: `callygonzo`,
    },
  },
  pathPrefix: `personal-site`,
  plugins: [
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        baseUrl: `callygonzo.wordpress.com`,
        protocol: `https`,
        hostingWPCOM: true,
        useACF: false,
        verbose: true,
      },
    },
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        path: `${__dirname}/content/assets`,
        name: `assets`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        //trackingId: `ADD YOUR TRACKING ID HERE`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Gatsby Starter Blog`,
        short_name: `GatsbyJS`,
        start_url: `/`,
        background_color: `#ffffff`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `content/assets/gatsby-icon.png`,
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
  ],
}
