import React from "react"
import { Link, graphql } from "gatsby"

import Bio from "../components/bio"
import Layout from "../components/layout"
import SEO from "../components/seo"
import { rhythm } from "../utils/typography"

const BlogIndex = ({ data, location }) => {
  const siteTitle = data.site.siteMetadata.title
  const posts = data.allWordpressPost.edges

  return (
    <Layout location={location} title={siteTitle}>
      <SEO title="Latest posts" />
      <Bio />
      {posts.map(({ node: { date, excerpt, slug, title } }) => (
        <article key={slug}>
          <header>
            <h3
              style={{
                marginBottom: rhythm(1 / 4),
              }}
            >
              <Link style={{ boxShadow: `none` }} to={slug}>
                {title}
              </Link>
            </h3>
            <small>{date}</small>
          </header>
          <section>
            <div
              dangerouslySetInnerHTML={{
                __html: excerpt,
              }}
            />
          </section>
        </article>
      ))}
    </Layout>
  )
}

export default BlogIndex

export const pageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    allWordpressPost(sort: { fields: date, order: DESC }) {
      edges {
        node {
          excerpt
          slug
          date(formatString: "MMMM DD, YYYY")
          title
          content
        }
      }
    }
  }
`
