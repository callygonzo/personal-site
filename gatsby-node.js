const path = require(`path`)
const slash = require(`slash`)
const { createFilePath } = require(`gatsby-source-filesystem`)

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions

  const blogPost = path.resolve(`./src/templates/blog-post.js`)
  // query content for WordPress posts
  const result = await graphql(
    `
      query {
        allWordpressPost {
          edges {
            next {
              id
              slug
              title
            }
            node {
              id
              slug
              title
            }
            previous {
              id
              slug
              title
            }
          }
        }
      }
    `
  )

  if (result.errors) {
    throw result.errors
  }

  // Create blog posts pages.
  const posts = result.data.allWordpressPost.edges

  posts.forEach((post, index) => {
    createPage({
      path: post.node.slug,
      component: slash(blogPost),
      context: {
        id: post.node.id,
        slug: post.node.slug,
        previous: post.previous,
        next: post.next,
      },
    })
  })
}

exports.onCreateNode = ({ node, actions, getNode }) => {
  const { createNodeField } = actions

  if (node.internal.type === `MarkdownRemark`) {
    const value = createFilePath({ node, getNode })
    createNodeField({
      name: `slug`,
      node,
      value,
    })
  }
}
